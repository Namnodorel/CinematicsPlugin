package de.namnodorel.cinematics;


import de.namnodorel.cinematics.logic.CameraPath;
import de.namnodorel.cinematics.logic.CameraPosition;
import de.namnodorel.cinematics.logic.PathCalculator;
import de.namnodorel.cinematics.logic.Scene;
import de.namnodorel.cinematics.utils.EntityHider;
import de.namnodorel.cinematics.utils.PacketHelper;
import net.citizensnpcs.api.CitizensAPI;
import net.citizensnpcs.api.npc.NPC;
import net.citizensnpcs.npc.CitizensNPCRegistry;
import net.citizensnpcs.util.NMS;
import net.minecraft.server.v1_11_R1.EntityPlayer;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_11_R1.CraftWorld;
import org.bukkit.craftbukkit.v1_11_R1.entity.CraftEntity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerTeleportEvent;
import org.bukkit.util.Vector;

import java.util.*;

public class SceneViewer {

    private static HashMap<String, Integer> pathIndexes = new HashMap<>();
    private static HashMap<String, Integer> positionIndexes = new HashMap<>();
    private static HashMap<String, Integer> taskIds = new HashMap<>();
    private static HashMap<String, Integer> cameraEntityIds = new HashMap<>();

    private static HashSet<String> playersWithTeleportPacketsDisallowed = new HashSet<>();
    private static HashSet<String> playersPaused = new HashSet<>();

    public static void playScene(Scene scene, Player p) {

        if(isViewingPath(p)){
            throw new IllegalArgumentException("Player " + p.getName() + " is still viewing a path(positionindex " + positionIndexes.get(p.getName()) + "), can't start viewing another one!");
        }

        //Preperation
        NPC cameraNPC = CitizensAPI.getNPCRegistry().createNPC(EntityType.PLAYER, "");
        cameraNPC.spawn(scene.getPaths().get(0).getPosition(0).getLocation());
        ((Player)cameraNPC.getEntity()).setGameMode(GameMode.SPECTATOR);

        EntityHider hider = new EntityHider(Cinematics.getInstance(), EntityHider.Policy.BLACKLIST);
        for (Player player : Bukkit.getServer().getOnlinePlayers()) {
            if (!player.equals(p)) {
                hider.hideEntity(player, cameraNPC.getEntity());
            }
        }


        p.setGameMode(GameMode.SPECTATOR);
        p.teleport(cameraNPC.getEntity(), PlayerTeleportEvent.TeleportCause.PLUGIN);
        p.setSpectatorTarget(cameraNPC.getEntity());
        p.setAllowFlight(true);

        pathIndexes.put(p.getName(), 0);
        playPath(scene, scene.getPaths().get(0), cameraNPC, p, new Runnable() {
            @Override
            public void run() {
                pathIndexes.put(p.getName(), pathIndexes.get(p.getName()) + 1);

                if(pathIndexes.get(p.getName()) < scene.getPaths().size()){

                    p.setSpectatorTarget(null);

                    cameraNPC.getEntity().teleport(scene.getPaths().get(pathIndexes.get(p.getName())).getPosition(0).getLocation(), PlayerTeleportEvent.TeleportCause.PLUGIN);
                    p.teleport(scene.getPaths().get(pathIndexes.get(p.getName())).getPosition(0).getLocation(), PlayerTeleportEvent.TeleportCause.PLUGIN);

                    NMS.look(cameraNPC.getEntity(), scene.getPaths().get(pathIndexes.get(p.getName())).getPosition(0).getLocation().getYaw(), scene.getPaths().get(pathIndexes.get(p.getName())).getPosition(0).getLocation().getPitch());

                    ((Player) cameraNPC.getEntity()).setSpectatorTarget(p);


                    playPath(scene, scene.getPaths().get(pathIndexes.get(p.getName())), cameraNPC, p, this, 2);
                }else{

                    stopViewingScene(p);

                    scene.getOnFinish().runActions(cameraNPC, p);

                }
            }
        }, 20);
    }

    private static void playPath(Scene fromScene, CameraPath path, NPC cameraNPC, Player p, final Runnable onFinish, long waitFor){

        //FIXME Still pitch spikes

        if(isViewingPath(p)){
            throw new IllegalArgumentException("Player " + p.getName() + " is still viewing a path(positionindex " + positionIndexes.get(p.getName()) + "), can't start viewing another one!");
        }

        if(!positionIndexes.containsKey(p.getName())){
            positionIndexes.put(p.getName(), 0);
        }


        final List<Location> locs = PathCalculator.calculateLocations(path.getPositions(), 20);
        final List<Vector> vels = new ArrayList<>();

        for(Location l : locs){
            vels.add(getVector(locs.indexOf(l) > 0 ? locs.get(locs.indexOf(l) - 1).clone() : p.getLocation(), l, 1));
        }

        Bukkit.getScheduler().scheduleSyncDelayedTask(Cinematics.getInstance(), () -> ((Player)cameraNPC.getEntity()).setSpectatorTarget(null), 1L);

        taskIds.put(p.getName(), Bukkit.getScheduler().scheduleSyncRepeatingTask(Cinematics.getInstance(), () -> {


            //Something went wrong, abort
            if(!positionIndexes.containsKey(p.getName())){
                stopViewingPath(p);
                fromScene.getOnInterrupted().runActions(cameraNPC, p);
                return;
            }

            if (positionIndexes.get(p.getName()) < locs.size()) {

                ((Player)cameraNPC.getEntity()).setGameMode(GameMode.SPECTATOR);
                p.setSpectatorTarget(cameraNPC.getEntity());
                net.minecraft.server.v1_11_R1.EntityPlayer cameraHandle = (EntityPlayer)((CraftEntity)cameraNPC.getEntity()).getHandle();
                cameraHandle.noclip = true;

                //The server would constantly be sending teleport packets to the player, which sometimes makes the camera flinch
                //To avoid this, we block off all teleport packages except when the camera explicitly needs to be teleported.
                //We allow it a little earlier to ensure the value is actually true when the camera should be teleported
                cameraEntityIds.put(p.getName(), cameraNPC.getEntity().getEntityId());
                if(positionIndexes.get(p.getName()) > locs.size() - 2 || positionIndexes.get(p.getName()) > 2){
                    playersWithTeleportPacketsDisallowed.remove(p.getName());
                }else{
                    playersWithTeleportPacketsDisallowed.add(p.getName());
                }

                if(!playersPaused.contains(p.getName())){

                    Location l = locs.get(positionIndexes.get(p.getName())).clone();
                    NMS.look(cameraNPC.getEntity(), l.getYaw(), l.getPitch());

                    Vector v = vels.get(positionIndexes.get(p.getName()));
                    cameraNPC.getEntity().setVelocity(v);

                    int ticks = 0;
                    for(CameraPosition pos : path.getPositions()){
                        if(positionIndexes.get(p.getName()) == ticks){

                            if(pos.getActions() != null && !pos.getActions().isEmpty()){
                                pos.runActions(cameraNPC, p);
                            }

                            break;
                        }
                        ticks += pos.getTimeUntilNext()*20;
                    }

                    PacketHelper.packetUpdate(cameraNPC);

                    positionIndexes.put(p.getName(), positionIndexes.get(p.getName()) + 1);
                }

            } else{
                stopViewingPath(p);

                onFinish.run();
            }

        //Due to some bug, in the first second after spawn the NPCs pitch stays at 0, so we wait that out
        }, waitFor, 1L));
    }

    public static void stopViewingScene(Player p){
        stopViewingPath(p);

        CitizensAPI.getNPCRegistry().getNPC(((CraftWorld)p.getWorld()).getHandle().getEntity(cameraEntityIds.get(p.getName())).getBukkitEntity()).destroy();
        pathIndexes.remove(p.getName());
        cameraEntityIds.remove(p.getName());
    }

    private static void stopViewingPath(Player p){
        if(taskIds.containsKey(p.getName())){
            Bukkit.getScheduler().cancelTask(taskIds.get(p.getName()));
            taskIds.remove(p.getName());
        }

        ((CraftWorld)p.getWorld()).getHandle().getEntity(cameraEntityIds.get(p.getName())).getBukkitEntity().setVelocity(new Vector(0, 0, 0));

        positionIndexes.remove(p.getName());
        playersWithTeleportPacketsDisallowed.remove(p.getName());
        playersPaused.remove(p.getName());

        p.setSpectatorTarget(null);
        p.setVelocity(new Vector(0, 0, 0));

    }

    public static boolean isViewingPath(Player player){
        return positionIndexes.containsKey(player.getName());
    }

    public static void pause(Player pausedPlayer){
        playersPaused.add(pausedPlayer.getName());
    }

    public static void resume(Player resumedPlayer){
        playersPaused.remove(resumedPlayer.getName());
    }

    public static boolean isPaused(Player player){
        return playersPaused.contains(player.getName());
    }

    static boolean isTeleportPacketAllowed(Integer entityId, Player player){
        if(cameraEntityIds.containsKey(player.getName())
                && cameraEntityIds.get(player.getName()).equals(entityId)
                && !playersWithTeleportPacketsDisallowed.contains(player.getName())){
            return false;
        }
        return true;
    }

    public static Integer getPositionIndexOf(String player){
        return positionIndexes.get(player);
    }

    public static void clear(){

        List<String> players = new ArrayList<>();

        for(String player : pathIndexes.keySet()){
            players.add(player);
        }

        for(String player : players){
            stopViewingScene(Bukkit.getPlayer(player));
        }
    }

    private static Vector getVector(Location p1, Location p2, double t) {
        double x = p2.getX() - p1.getX();
        double y = p2.getY() - p1.getY();
        double z = p2.getZ() - p1.getZ();

        double valX = x / t;
        double valY = y / t;
        double valZ = z / t;

        return new Vector(valX, valY, valZ);
    }
}
