package de.namnodorel.cinematics;

import com.comphenix.packetwrapper.*;
import com.comphenix.protocol.PacketType;
import com.comphenix.protocol.ProtocolLibrary;
import com.comphenix.protocol.events.*;
import com.comphenix.protocol.injector.packet.PacketRegistry;
import de.namnodorel.cinematics.logic.CameraPath;
import de.namnodorel.cinematics.logic.CameraPosition;
import de.namnodorel.cinematics.logic.Scene;
import de.namnodorel.cinematics.setup.CinematicCommand;
import de.namnodorel.cinematics.utils.PacketHelper;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.configuration.serialization.ConfigurationSerialization;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public final class Cinematics extends JavaPlugin {

    private static Cinematics instance;

    @Override
    public void onEnable() {
        instance = this;

        this.getCommand("cinematic").setExecutor(new CinematicCommand());

        ConfigurationSerialization.registerClass(Scene.class);
        ConfigurationSerialization.registerClass(CameraPath.class);
        ConfigurationSerialization.registerClass(CameraPosition.class);


        SceneManager.reload();


        ProtocolLibrary.getProtocolManager().addPacketListener(new PacketAdapter(this, ListenerPriority.LOWEST,
                PacketType.Play.Server.SPAWN_ENTITY_LIVING,
                PacketType.Play.Server.REL_ENTITY_MOVE_LOOK,
                PacketType.Play.Server.ENTITY_LOOK,
                PacketType.Play.Server.ENTITY_TELEPORT,
                PacketType.Play.Server.ENTITY_HEAD_ROTATION) {
            @Override
            public void onPacketSending(PacketEvent event) {

                if(event.getPacketType().equals(PacketType.Play.Server.ENTITY_TELEPORT)){
                    //The server would constantly be sending teleport packets to the player, which sometimes makes the camera flinch
                    //To avoid this, we block off all teleport packages except when the camera explicitly needs to be teleported.
                    WrapperPlayServerEntityTeleport packet = new WrapperPlayServerEntityTeleport(event.getPacket());
                    if(!SceneViewer.isTeleportPacketAllowed(packet.getEntityID(), event.getPlayer())){
                        event.setCancelled(true);
                    }
                }

            }
        });

        this.getServer().getPluginManager().registerEvents(new CinematicListener(), this);
    }

    @Override
    public void onDisable() {
        super.onDisable();

        SceneManager.save();
    }

    public static Cinematics getInstance() {
        return instance;
    }
}
