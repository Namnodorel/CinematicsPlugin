package de.namnodorel.cinematics;

import de.namnodorel.cinematics.logic.*;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;

public class SceneManager {

    private static HashMap<String, Scene> scenes = new HashMap<>();

    private static HashMap<String, Scene> sceneSelections = new HashMap<>();
    private static HashMap<String, CameraPath> pathSelections = new HashMap<>();
    private static HashMap<String, CameraPosition> positionSelections = new HashMap<>();
    private static HashMap<String, Action> actionSelections = new HashMap<String, Action>();

    public static void addScene(Scene s) {
        if (!sceneExists(s.getName())) {
            scenes.put(s.getName(), s);
        } else {
            throw new IllegalArgumentException("A scene with the name'" + s.getName() + "' already exists!");
        }
    }

    public static Scene getScene(String sceneName) {
        if (sceneExists(sceneName)) {
            return scenes.get(sceneName);
        } else {
            throw new IllegalArgumentException("There exists no scene with the given name: " + sceneName);
        }
    }

    public static void removeScene(String sceneName) {
        if (sceneExists(sceneName)) {
            scenes.remove(sceneName);
        } else {
            throw new IllegalArgumentException("There exists no scene with the given name: " + sceneName);
        }
    }

    public static boolean sceneExists(String sceneName) {
        return scenes.containsKey(sceneName);
    }

    public static Scene getSelectedScene(String player) {
        return sceneSelections.getOrDefault(player, null);
    }

    public static CameraPath getSelectedPath(String player) {
        return pathSelections.getOrDefault(player, null);
    }

    public static CameraPosition getSelectedPosition(String player) {
        return positionSelections.getOrDefault(player, null);
    }

    public static Action getSelectedAction(String player){
        return actionSelections.getOrDefault(player, null);
    }

    public static void selectScene(String player, Scene s) {

        if (s == null) {

            if (sceneSelections.containsKey(player)) {
                sceneSelections.remove(player);
            }

            return;
        }

        if (!sceneExists(s.getName())) {
            throw new IllegalArgumentException("There exists no scene with the given name: " + s.getName());
        }

        sceneSelections.put(player, s);

        //Automatically select the first or no path of the selected scene
        if (s.hasPaths()) {
            selectPath(player, s.getPaths().get(0).getName());
        } else {
            selectPath(player, null);
        }
    }

    public static void selectPath(String player, String pathName) {

        Scene scene = getSelectedScene(player);

        if (scene == null) {
            throw new IllegalArgumentException("There is no scene selected, can't select a path!");
        }

        if (pathName == null) {
            if (pathSelections.containsKey(player)) {
                pathSelections.remove(player);
            }

            selectPosition(player, null);
        } else if (scene.containsPath(pathName)) {
            pathSelections.put(player, scene.getPath(pathName));

            //Automatically select the first or no position of the selected path
            if (scene.getPath(pathName).getPositions().size() > 0) {
                selectPosition(player, 0);
            } else {
                selectPosition(player, null);
            }
        } else {
            throw new IllegalArgumentException("There exists no path with the given name: " + pathName);
        }

    }

    public static void selectPosition(String player, Integer positionIndex) {

        if (positionIndex == null) {
            if (positionSelections.containsKey(player)) {
                positionSelections.remove(player);
            }
            return;
        }

        Scene scene = getSelectedScene(player);
        if (scene == null) {
            throw new IllegalArgumentException("There is no scene selected, can't select a position!");
        }

        CameraPath path = getSelectedPath(player);
        if (path == null) {
            throw new IllegalArgumentException("There is no path selected, can't select a position!");
        }

        positionSelections.put(player, path.getPosition(positionIndex));

    }

    public static void selectAction(String player, Integer actionIndex, ActionHolder actionHolder) {

        if(actionIndex == null){
            if(actionSelections.containsKey(player)){
                actionSelections.remove(player);
            }
            return;
        }

        actionSelections.put(player, actionHolder.getAction(actionIndex));
    }

    public static HashMap<String, Scene> getScenes() {
        return scenes;
    }

    public static void reload(){

        SceneViewer.clear();

        scenes = new HashMap<>();
        sceneSelections = new HashMap<>();
        pathSelections = new HashMap<>();
        positionSelections = new HashMap<>();
        actionSelections = new HashMap<>();

        FileConfiguration data = YamlConfiguration.loadConfiguration(new File(Cinematics.getInstance().getDataFolder(), "cinematics.yml"));
        ConfigurationSection section = data.getConfigurationSection("cinematics");
        if(section != null){
            HashMap<String, Object> serializedScenes = (HashMap<String, Object>)section.getValues(false);
            for(String sceneName : serializedScenes.keySet()){
                SceneManager.addScene((Scene)serializedScenes.get(sceneName));
            }
        }
    }

    public static void save(){
        FileConfiguration data = YamlConfiguration.loadConfiguration(new File(Cinematics.getInstance().getDataFolder(), "cinematics.yml"));
        data.createSection("cinematics", SceneManager.getScenes());
        try {
            data.save(new File(Cinematics.getInstance().getDataFolder(), "cinematics.yml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
