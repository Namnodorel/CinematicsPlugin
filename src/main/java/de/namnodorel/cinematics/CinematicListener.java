package de.namnodorel.cinematics;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

public class CinematicListener implements Listener {

    @EventHandler
    public void onPlayerQuit(PlayerQuitEvent event){
        if(SceneViewer.isViewingPath(event.getPlayer())){
            SceneViewer.stopViewingScene(event.getPlayer());
        }
    }

}
