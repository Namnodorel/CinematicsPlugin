package de.namnodorel.cinematics.setup;

import de.namnodorel.cinematics.SceneManager;
import de.namnodorel.cinematics.SceneViewer;
import de.namnodorel.cinematics.actions.ChangeGamemodeAction;
import de.namnodorel.cinematics.actions.ExecuteCommandAction;
import de.namnodorel.cinematics.actions.TeleportPlayerAction;
import de.namnodorel.cinematics.logic.*;
import de.namnodorel.cinematics.utils.Utils;
import de.namnodorel.messageutils.MessageBlock;
import de.namnodorel.messageutils.message.FancyMessage;
import org.apache.commons.lang.StringUtils;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class CinematicCommand implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

        //TODO Better identifiable shortcuts (?)

        if(!sender.isOp()){
            sender.sendMessage("§cDu hast nicht die Berechtigung dazu, diesen Befehl zu nutzen.");
            return true;
        }

        Player p = null;

        if(sender instanceof Player){
            p = (Player)sender;
        }

        if(args.length == 0){
            sender.sendMessage("§cBitte gib weitere Argumente an!");
            return true;
        }

        if(args[0].equalsIgnoreCase("create") || args[0].equalsIgnoreCase("c")){

            if(args.length >= 2){
                if(args[1].equalsIgnoreCase("scene") || args[1].equalsIgnoreCase("s")){

                    if(args.length == 3){

                        String sceneName = args[2];
                        if(SceneManager.sceneExists(sceneName)){
                            sender.sendMessage("§cDiese Szene existert bereits!");
                        }else{
                            Scene scene = new Scene(sceneName);
                            SceneManager.addScene(scene);
                            SceneManager.selectScene(sender.getName(), scene);
                            sender.sendMessage("§aDie Szene §e" + sceneName + "§a wurde hinzugefügt und ausgewählt!");
                        }

                    }else{
                        sender.sendMessage("§c/cinematic create scene <name>");
                    }

                }else if(args[1].equalsIgnoreCase("path") || args[1].equalsIgnoreCase("p")){

                    if(args.length == 3){

                        Scene selectedScene = SceneManager.getSelectedScene(sender.getName());
                        if(selectedScene != null){

                            String pathName = args[2];

                            if(selectedScene.containsPath(pathName)){
                                sender.sendMessage("§cDieser Pfad existiert bereits in dieser Szene!");
                            }else{
                                CameraPath path = new CameraPath(pathName);
                                selectedScene.addPath(path);
                                SceneManager.selectPath(sender.getName(), pathName);
                                sender.sendMessage("§aDer Pfad §e" + selectedScene.getName() + ">" + pathName + "§a wurde hinzugefügt und ausgewählt!");
                            }

                        }else{
                            sender.sendMessage("§cDu musst zuerst eine Szene auswählen, zu der du den Pfad hinzufügen möchtest!");
                        }

                    }else{
                        sender.sendMessage("§c/cinematic create path <name>");
                    }

                }else if(args[1].equalsIgnoreCase("position") || args[1].equalsIgnoreCase("pos")){

                    if(p != null){
                        if(args.length == 2 || args.length == 3){


                            CameraPath path = SceneManager.getSelectedPath(p.getName());

                            if(path != null) {

                                Double timeSeconds = 1.0;
                                if(args.length == 3){
                                    if(Utils.isDouble(args[2])){
                                        timeSeconds = Double.valueOf(args[2]);
                                    }else{
                                        sender.sendMessage("§cBitte gib einen validen Wert als Zeit ein, Beispielsweise 3.5 oder 2.0");
                                        return true;
                                    }
                                }

                                CameraPosition pos = new CameraPosition(p.getLocation().clone(), timeSeconds);
                                Integer index = path.addPosition(pos);
                                SceneManager.selectPosition(p.getName(), index);
                                p.sendMessage("§aDie Position §e" + SceneManager.getSelectedScene(p.getName()).getName() + ">" + path.getName()+ ">" + index + "§a wurde hinzugefügt und ausgewählt!");

                            }else{
                                p.sendMessage("§cDu musst zuerst einen Pfad auswählen, zu dem du die neue Position hinzufügst!");
                            }

                        }else{
                            sender.sendMessage("§c/cinematic create position [secondsUntilNext]");
                        }
                    }else{
                        sender.sendMessage("§cDieser Befehl ist nur für Spieler!");
                    }

                }else if(args[1].equalsIgnoreCase("action") || args[1].equalsIgnoreCase("a")){

                    if(args.length >= 4){

                        ActionHolder actionHolder = null;
                        String positionString = "";

                        if(args[2].equalsIgnoreCase("atposition") || args[2].equalsIgnoreCase("ap")){
                            CameraPath path = SceneManager.getSelectedPath(sender.getName());
                            if(path != null){
                                CameraPosition pos = SceneManager.getSelectedPosition(sender.getName());

                                if(pos != null){
                                    actionHolder = pos;
                                    positionString = "Position " + path.getPositions().indexOf(pos);
                                }else{
                                    sender.sendMessage("§cDu musst zuerst eine Position ausgewählt haben!");
                                }

                            }else{
                                sender.sendMessage("§cDu musst zuerst einen Pfad ausgewählt haben!");
                            }
                        }else if(args[2].equalsIgnoreCase("onFinish") || args[2].equalsIgnoreCase("of")){

                            Scene scene = SceneManager.getSelectedScene(sender.getName());
                            if(scene != null){
                                actionHolder = scene.getOnFinish();
                                positionString = "erfolgreichem Abschluss der Szene";
                            }else{
                                sender.sendMessage("§cDu musst zuerst eine Szene ausgewählt haben!");
                            }

                        }else if(args[2].equalsIgnoreCase("onInterrupted") || args[2].equalsIgnoreCase("oi")){
                            Scene scene = SceneManager.getSelectedScene(sender.getName());
                            if(scene != null){
                                actionHolder = scene.getOnInterrupted();
                                positionString = "Störung der Szene";
                            }else{
                                sender.sendMessage("§cDu musst zuerst eine Szene ausgewählt haben!");
                            }
                        }else{
                            sender.sendMessage("§c/cinematic create action <atposition/onFinish/onInterrupted> <action>");
                        }

                        if(actionHolder != null){
                            if(args[3].equalsIgnoreCase("teleport") || args[3].equalsIgnoreCase("tp")) {

                                if (p != null) {

                                    if (args.length == 5) {

                                        TeleportPlayerAction tpAction = new TeleportPlayerAction(args[4], p.getLocation().clone());
                                        actionHolder.addAction(tpAction);
                                        sender.sendMessage("§aDie Aktion §eTeleportiere " + args[4] + " bei " + positionString + " zu deiner aktuellen Position§a wurde hinzugefügt!");

                                    } else {
                                        sender.sendMessage("§c/cinematic create action <atposition/onFinish/onInterrupted> teleport <player/VIEWER/NPC:<ID>>");
                                    }

                                } else {
                                    sender.sendMessage("§cDieser Befehl ist nur für Spieler!");
                                }
                            }else if(args[3].equalsIgnoreCase("gamemode") || args[3].equalsIgnoreCase("gm")) {

                                if (args.length == 6) {

                                    GameMode gameMode = null;
                                    if (args[4].equalsIgnoreCase("0") || args[4].equalsIgnoreCase("survival")) {
                                        gameMode = GameMode.SURVIVAL;
                                    } else if (args[4].equalsIgnoreCase("1") || args[4].equalsIgnoreCase("creative")) {
                                        gameMode = GameMode.CREATIVE;
                                    } else if (args[4].equalsIgnoreCase("2") || args[4].equalsIgnoreCase("adventure")) {
                                        gameMode = GameMode.ADVENTURE;
                                    } else if (args[4].equalsIgnoreCase("3") || args[4].equalsIgnoreCase("spectator")) {
                                        gameMode = GameMode.SPECTATOR;
                                    } else {
                                        sender.sendMessage("§cBitte gib einen gültigen Spielmodus an! <survival/creative/adventure/spectator>");
                                    }

                                    if (gameMode != null) {
                                        ChangeGamemodeAction gamemodeAction = new ChangeGamemodeAction(args[5], gameMode);
                                        actionHolder.addAction(gamemodeAction);
                                        sender.sendMessage("§aDie Aktion §eÄndere den Spielmodus von " + args[5] + " zu " + args[4] + " bei " + positionString + " §awurde hinzugefügt!");
                                    }

                                } else {
                                    sender.sendMessage("§c/cinematic create action <atposition/onFinish/onInterrupted> gamemode <gamemode> <player/VIEWER/CONSOLER/NPC:<ID>>");
                                }
                            }else if(args[3].equalsIgnoreCase("command") || args[3].equalsIgnoreCase("cmd")){

                                if(args.length >= 6){

                                    String executor = args[4];
                                    StringBuilder command = new StringBuilder();
                                    for(String arg : Arrays.copyOfRange(args, 5, args.length)){
                                        command.append(" ").append(arg);
                                    }
                                    ExecuteCommandAction cmdAction = new ExecuteCommandAction(executor, command.toString());
                                    actionHolder.addAction(cmdAction);
                                    sender.sendMessage("§aDie Aktion §eFühre bei " + positionString + " als " + executor + " den Befehl " + command + " aus§a wurde hinzugefügt!");

                                }else{
                                    sender.sendMessage("§c/cinematic create action <atposition/onFinish/onInterrupted> command <executor> <command>");
                                }

                            }else{
                                sender.sendMessage("§cUnbekannte Aktion!");
                            }
                        }

                    }else{
                        sender.sendMessage("§c/cinematic create action <atposition/onFinish/onInterrupted> <action>");
                    }

                }else{
                    sender.sendMessage("§c/cinematic create <scene/path/position/action>");
                }
            }else{
                sender.sendMessage("§c/cinematic create <scene/path/position/action>");
            }

        }else if(args[0].equalsIgnoreCase("list") || args[0].equalsIgnoreCase("l")){

            if(args.length >= 2){
                if(args[1].equalsIgnoreCase("scenes") || args[1].equalsIgnoreCase("s")){
                    sender.sendMessage("§aFolgende Szenen sind auf dem Server:");

                    HashMap<String, Scene> scenes = SceneManager.getScenes();
                    if(scenes.size() == 0){
                        sender.sendMessage("§cKeine");
                    }else{
                        if(p != null){
                            MessageBlock msgBlock = new MessageBlock(6);

                            for(String sceneName : scenes.keySet()){

                                String selected = "";
                                if(SceneManager.getSelectedScene(p.getName()) != null && SceneManager.getSelectedScene(p.getName()).getName().equalsIgnoreCase(sceneName)){
                                    selected = "§l";
                                }

                                msgBlock.addMessage(new FancyMessage("§e" + selected + sceneName)
                                        .command("/cinematic select scene " + sceneName));
                            }

                            MessageBlock.sendMessagesToPlayer(p, msgBlock);
                        }else{
                            for(String sceneName : scenes.keySet()){
                                String selected = "";
                                if(SceneManager.getSelectedScene(sender.getName()) != null && SceneManager.getSelectedScene(p.getName()).getName().equalsIgnoreCase(sceneName)){
                                    selected = "§l";
                                }

                                sender.sendMessage("§e" + selected + sceneName);
                            }
                        }
                    }
                }else if(args[1].equalsIgnoreCase("paths") || args[1].equalsIgnoreCase("p")){

                    Scene selectedScene = SceneManager.getSelectedScene(sender.getName());
                    if(selectedScene != null){

                        sender.sendMessage("§aPfade der Szene §e" + selectedScene.getName());

                        List<CameraPath> paths = selectedScene.getPaths();
                        if(paths.size() == 0){
                            sender.sendMessage("§cKeine");
                        }else{

                            if(p != null){
                                MessageBlock msgBlock = new MessageBlock(6);

                                for(CameraPath path : paths){

                                    String selected = "";
                                    if(SceneManager.getSelectedPath(p.getName()) !=  null && SceneManager.getSelectedPath(p.getName()) == path){
                                        selected = "§l";
                                    }

                                    msgBlock.addMessage(new FancyMessage("§e" + selected + path.getName())
                                            .command("/cinematic select path " + path.getName()));
                                }

                                MessageBlock.sendMessagesToPlayer(p, msgBlock);
                            }else{
                                for(CameraPath path : paths){
                                    String selected = "";
                                    if(SceneManager.getSelectedPath(sender.getName()) !=  null && SceneManager.getSelectedPath(sender.getName()) == path){
                                        selected = "§l";
                                    }

                                    sender.sendMessage("§e" + selected + path.getName());
                                }
                            }
                        }

                    }else{
                        sender.sendMessage("§cDu musst zuerst eine Szene auswählen, bevor du dir ihre Pfade ansehen kannst!");
                    }

                }else if(args[1].equalsIgnoreCase("positions") || args[1].equalsIgnoreCase("pos")){

                    Scene selectedScene = SceneManager.getSelectedScene(sender.getName());
                    if(selectedScene != null){

                        CameraPath selectedPath = SceneManager.getSelectedPath(sender.getName());
                        if(selectedPath != null){

                            List<CameraPosition> positions = selectedPath.getPositions();

                            sender.sendMessage("§aDie folgenden Positionen befinden sich im Pfad §e" + selectedPath.getName() + ":");
                            if(positions.size() == 0){
                                sender.sendMessage("§cKeine");
                            }else{

                                if(p != null){
                                    MessageBlock msgBlock = new MessageBlock(6);

                                    for(CameraPosition pos : positions){

                                        String selected = "";
                                        if(SceneManager.getSelectedPosition(sender.getName()) !=  null && SceneManager.getSelectedPosition(sender.getName()) == pos){
                                            selected = "§l";
                                        }

                                        Location loc = pos.getLocation();
                                        msgBlock.addMessage(new FancyMessage( "§a" + selected + positions.indexOf(pos) + " §e" + selected + "X:" + loc.getX() + " Y:" + loc.getY() + " Z:" + loc.getZ() + " Yaw:" + loc.getYaw() + " Pitch:" + loc.getPitch())
                                                .command("/cinematic select position " + positions.indexOf(pos))
                                                .then("§e[§cTP§e]")
                                                .command("/tp " + p.getName() + " " + loc.getX() + " " + loc.getY() + " " + loc.getZ() + " " + loc.getYaw() + " " + loc.getPitch())
                                        );
                                    }

                                    MessageBlock.sendMessagesToPlayer(p, msgBlock);
                                }else{
                                    for(CameraPosition pos : positions){
                                        String selected = "";
                                        if(SceneManager.getSelectedPosition(sender.getName()) !=  null && SceneManager.getSelectedPosition(sender.getName()) == pos){
                                            selected = "§l";
                                        }

                                        Location loc = pos.getLocation();
                                        sender.sendMessage("§a" + selected + positions.indexOf(pos) + " §e" + selected + "X:" + loc.getX() + " Y:" + loc.getY() + " Z:" + loc.getZ() + " Yaw:" + loc.getYaw() + " Pitch:" + loc.getPitch());
                                    }
                                }
                            }

                        }else{
                            sender.sendMessage("§cDu musst zuerst einen Pfad auswählen, bevor du dir seine Positionen ansehen kannst!");
                        }

                    }else{
                        sender.sendMessage("§cDu musst zuerst eine Szene auswählen, bevor du die Positionen ihrer Pfade ansehen kannst!");
                    }

                }else if(args[1].equalsIgnoreCase("actions") || args[1].equalsIgnoreCase("a")){

                    ActionHolder actionHolder = null;
                    String actionHolderString = "?";

                    if(args.length >= 3){
                        if(args[2].equalsIgnoreCase("atposition") || args[2].equalsIgnoreCase("ap")){
                            CameraPath path = SceneManager.getSelectedPath(sender.getName());
                            if(path != null){

                                CameraPosition pos = SceneManager.getSelectedPosition(sender.getName());
                                if(pos != null){
                                    actionHolder = pos;
                                    actionHolderString = "atposition";
                                }else{
                                    sender.sendMessage("§cDu musst zuerst eine Position auswählen!");
                                }

                            }else{
                                sender.sendMessage("§cDu musst zuerst einen Pfad auswählen!");
                            }
                        }else if(args[2].equalsIgnoreCase("onFinish") || args[2].equalsIgnoreCase("of")){

                            Scene scene = SceneManager.getSelectedScene(sender.getName());
                            if(scene != null){
                                actionHolder = scene.getOnFinish();
                                actionHolderString = "onFinish";
                            }else{
                                sender.sendMessage("§cDu musst zuerst eine Szene ausgewählt haben!");
                            }

                        }else if(args[2].equalsIgnoreCase("onInterrupted") || args[2].equalsIgnoreCase("oi")){

                            Scene scene = SceneManager.getSelectedScene(sender.getName());
                            if(scene != null){
                                actionHolder = scene.getOnInterrupted();
                                actionHolderString = "onInterrupted";
                            }else{
                                sender.sendMessage("§cDu musst zuerst eine Szene ausgewählt haben!");
                            }

                        }else{
                            sender.sendMessage("§c/cinematic list actions <atposition/onFinish/onInterrupted>");
                        }
                    }else{
                        sender.sendMessage("§c/cinematic list actions <atposition/onFinish/onInterrupted>");
                    }

                    if(actionHolder != null){
                        if(actionHolder.getActions().size() > 0){
                            if(p != null){

                                MessageBlock msgBlock = new MessageBlock(6);

                                for(Action action : actionHolder.getActions()){

                                    String selected = "";
                                    if(SceneManager.getSelectedAction(sender.getName()) !=  null && SceneManager.getSelectedAction(sender.getName()) == action){
                                        selected = "§l";
                                    }

                                    String desc = "?";
                                    if(action instanceof TeleportPlayerAction){
                                        TeleportPlayerAction tpPlayerAction = (TeleportPlayerAction)action;
                                        desc = "Teleport " + tpPlayerAction.getWhom() + " to X:" + tpPlayerAction.getLocation().getX() + " Y:" + tpPlayerAction.getLocation().getY() + " Z:" + tpPlayerAction.getLocation().getZ() + " Yaw:" + tpPlayerAction.getLocation().getYaw() + " Pitch:" + tpPlayerAction.getLocation().getPitch();
                                    }else if(action instanceof ChangeGamemodeAction){
                                        ChangeGamemodeAction gamemodeAction = (ChangeGamemodeAction)action;
                                        desc = "Gamemode " + gamemodeAction.getGamemode() + " for " + gamemodeAction.getWhom();
                                    }else if(action instanceof ExecuteCommandAction){
                                        ExecuteCommandAction cmdAction = (ExecuteCommandAction)action;
                                        desc = "Execute " + cmdAction.getCommand() + " as " + cmdAction.getAsWhom();
                                    }

                                    msgBlock.addMessage(new FancyMessage( "§a" + selected + actionHolder.getActions().indexOf(action) + ": §e" + desc)
                                            .command("/cinematic select action " + actionHolderString + " " + actionHolder.getActions().indexOf(action))
                                    );
                                }

                                MessageBlock.sendMessagesToPlayer(p, msgBlock);

                            }else{

                                for(Action action : actionHolder.getActions()){

                                    String selected = "";
                                    if(SceneManager.getSelectedAction(sender.getName()) !=  null && SceneManager.getSelectedAction(sender.getName()) == action){
                                        selected = "§l";
                                    }

                                    String desc = "?";
                                    if(action instanceof TeleportPlayerAction){
                                        TeleportPlayerAction tpPlayerAction = (TeleportPlayerAction)action;
                                        desc = "Teleport " + tpPlayerAction.getWhom() + " to X:" + tpPlayerAction.getLocation().getX() + " Y:" + tpPlayerAction.getLocation().getY() + " Z:" + tpPlayerAction.getLocation().getZ() + " Yaw:" + tpPlayerAction.getLocation().getYaw() + " Pitch:" + tpPlayerAction.getLocation().getPitch();
                                    }else if(action instanceof ChangeGamemodeAction){
                                        ChangeGamemodeAction gamemodeAction = (ChangeGamemodeAction)action;
                                        desc = "Gamemode " + gamemodeAction.getGamemode() + " for " + gamemodeAction.getWhom();
                                    }else if(action instanceof ExecuteCommandAction){
                                        ExecuteCommandAction cmdAction = (ExecuteCommandAction)action;
                                        desc = "Execute " + cmdAction.getCommand() + " as " + cmdAction.getAsWhom();
                                    }

                                    sender.sendMessage("§a" + selected + actionHolder.getActions().indexOf(action) + ": " + desc);
                                }

                            }
                        }else{
                            sender.sendMessage("§cKeine Aktionen gefunden!");
                        }
                    }

                }else{
                    sender.sendMessage("§c/cinematic list <scenes/paths/positions/actions>");
                }
            }else{
                sender.sendMessage("§c/cinematic list <scenes/paths/positions/actions>");
            }


        }else if(args[0].equalsIgnoreCase("select") || args[0].equalsIgnoreCase("s")) {

            if(args.length >= 2){
                if (args[1].equalsIgnoreCase("scene") || args[1].equalsIgnoreCase("s")) {
                    if (args.length == 3) {

                        if (SceneManager.sceneExists(args[2])) {

                            Scene scene = SceneManager.getScene(args[2]);
                            SceneManager.selectScene(sender.getName(), scene);
                            sender.sendMessage("§aDie Szene §e" + scene.getName() + " §awurde ausgewählt!");

                        } else {
                            sender.sendMessage("§cEs wurde keine Szene mit diesem Namen gefunden!");
                        }

                    } else {
                        sender.sendMessage("§c/cinematic select scene <scene>");
                    }
                } else if (args[1].equalsIgnoreCase("path") || args[1].equalsIgnoreCase("p")) {

                    if (args.length == 3) {

                        Scene selectedScene = SceneManager.getSelectedScene(sender.getName());
                        if (selectedScene != null) {

                            if (selectedScene.containsPath(args[2])) {

                                SceneManager.selectPath(sender.getName(), args[2]);
                                sender.sendMessage("§aDu hast den Pfad §e" + args[2] + " §aausgewählt!");

                            } else {
                                sender.sendMessage("§cDieser Pfad wurde nicht gefunden!");
                            }

                        } else {
                            sender.sendMessage("§cDu musst zuerst eine Szene auswählen!");
                        }

                    } else {
                        sender.sendMessage("§c/cinematic select path <path>");
                    }

                } else if (args[1].equalsIgnoreCase("position") || args[1].equalsIgnoreCase("pos")) {

                    if (args.length == 3) {

                        Scene selectedScene = SceneManager.getSelectedScene(sender.getName());
                        if (selectedScene != null) {

                            CameraPath selectedPath = SceneManager.getSelectedPath(sender.getName());
                            if (selectedPath != null) {

                                if (StringUtils.isNumeric(args[2])) {

                                    Integer index = Integer.valueOf(args[2]);
                                    if (index >= 0 && selectedPath.getPositions().size() - 1 >= index) {
                                        SceneManager.selectPosition(sender.getName(), index);
                                        sender.sendMessage("§aDu hast die Position §e" + selectedScene.getName() + ">" + selectedPath.getName() + ">" + index + " §aausgewählt!");
                                    } else {
                                        sender.sendMessage("§cEs gibt keine Position mit dieser Nummer!");
                                    }

                                } else {
                                    sender.sendMessage("§cBitte gib eine valide Zahl ein!");
                                }

                            } else {
                                sender.sendMessage("§cDu musst zuerst einen Pfad auswählen!");
                            }

                        } else {
                            sender.sendMessage("§cDu musst zuerst eine Szene auswählen!");
                        }

                    }else{
                        sender.sendMessage("§c/cinematic select position <index>");
                    }
                }else if (args[1].equalsIgnoreCase("action") || args[1].equalsIgnoreCase("a")){

                    if (args.length == 4) {

                        ActionHolder actionHolder = null;

                        if(args[2].equalsIgnoreCase("atposition")){

                            CameraPath selectedPath = SceneManager.getSelectedPath(sender.getName());
                            if (selectedPath != null) {

                                CameraPosition pos = SceneManager.getSelectedPosition(sender.getName());
                                if(pos != null){
                                    actionHolder = pos;
                                }else{
                                    sender.sendMessage("§cDu musst zuerst eine Position auswählen!");
                                }

                            } else {
                                sender.sendMessage("§cDu musst zuerst einen Pfad auswählen!");
                            }

                        }else if(args[2].equalsIgnoreCase("onFinish")){

                            Scene scene = SceneManager.getSelectedScene(sender.getName());
                            if(scene != null){
                                actionHolder = scene.getOnFinish();
                            }else{
                                sender.sendMessage("§cDu musst zuerst eine Szene ausgewählt haben!");
                            }

                        }else if(args[2].equalsIgnoreCase("onInterrupted")){

                            Scene scene = SceneManager.getSelectedScene(sender.getName());
                            if(scene != null){
                                actionHolder = scene.getOnInterrupted();
                            }else{
                                sender.sendMessage("§cDu musst zuerst eine Szene ausgewählt haben!");
                            }
                        }else{
                            sender.sendMessage("§c/cinematic select action <atposition/onFinish/onInterrupt> <index>");
                        }

                        if(actionHolder != null){
                            if (StringUtils.isNumeric(args[3])) {

                                Integer index = Integer.valueOf(args[3]);
                                if (index >= 0 && actionHolder.getActions().size() - 1 >= index) {
                                    SceneManager.selectAction(sender.getName(), index, actionHolder);
                                    sender.sendMessage("§aDu hast die Aktion §e" + index + " §aausgewählt!");
                                } else {
                                    sender.sendMessage("§cEs gibt keine Aktion mit dieser Nummer!");
                                }

                            } else {
                                sender.sendMessage("§cBitte gib eine valide Zahl ein!");
                            }
                        }

                    }else{
                        sender.sendMessage("§c/cinematic select action <atposition/onFinish/onInterrupt> <index>");
                    }

                } else if (args[1].equalsIgnoreCase("direct") || args[1].equalsIgnoreCase("d")) {

                    if (args.length >= 3 && args.length <= 5) {

                        if (SceneManager.sceneExists(args[2])) {

                            Scene scene = SceneManager.getScene(args[2]);
                            SceneManager.selectScene(sender.getName(), scene);


                            if (args.length >= 4) {

                                if (scene.containsPath(args[3])) {

                                    CameraPath path = scene.getPath(args[3]);

                                    SceneManager.selectPath(sender.getName(), path.getName());

                                    if (args.length == 5) {

                                        if (StringUtils.isNumeric(args[4])) {

                                            Integer index = Integer.valueOf(args[4]);
                                            if (index >= 0 && path.getPositions().size() - 1 >= index) {
                                                SceneManager.selectPosition(sender.getName(), index);

                                                sender.sendMessage("§aAusgewählt: §e" + scene.getName() + ">" + path.getName() + ">" + index);

                                            } else {
                                                sender.sendMessage("§cEs gibt keine Position mit dieser Nummer!");
                                            }

                                        } else {
                                            sender.sendMessage("§cBitte gib eine valide Zahl ein!");
                                        }

                                    } else {
                                        String position = "";

                                        if (SceneManager.getSelectedPosition(sender.getName()) != null) {
                                            position = ">" + path.getPositions().indexOf(SceneManager.getSelectedPosition(sender.getName()));
                                        }

                                        sender.sendMessage("§aAusgewählt: §e" + scene.getName() + ">" + path.getName() + position);
                                    }

                                } else {
                                    sender.sendMessage("§cDieser Pfad wurde nicht gefunden!");
                                }

                            } else {
                                String path = "";
                                String position = "";

                                if (SceneManager.getSelectedPath(sender.getName()) != null) {

                                    CameraPath pth = SceneManager.getSelectedPath(sender.getName());

                                    path = ">" + pth.getName();

                                    if (SceneManager.getSelectedPosition(sender.getName()) != null) {
                                        position = ">" + pth.getPositions().indexOf(SceneManager.getSelectedPosition(sender.getName()));
                                    }
                                }

                                sender.sendMessage("§aAusgewählt: §e" + scene.getName() + path + position);
                            }

                        } else {
                            sender.sendMessage("§cEs wurde keine Szene mit diesem Namen gefunden!");
                        }

                    } else {
                        sender.sendMessage("§c/cinematic select direct <scene> [path] [position]");
                    }

                }else{
                    sender.sendMessage("§c/cinematic select <scene/path/position/action/direct>");
                }
            }else{
                sender.sendMessage("§c/cinematic select <scene/path/position/action/direct>");
            }

        }else if(args[0].equalsIgnoreCase("edit") || args[0].equalsIgnoreCase("e")){

            if(args.length >= 2){
                if(args[1].equalsIgnoreCase("scene") || args[1].equalsIgnoreCase("s")){

                    if(args.length == 4){

                        if(args[2].equalsIgnoreCase("name") || args[2].equalsIgnoreCase("n")){
                            Scene selectedScene = SceneManager.getSelectedScene(sender.getName());
                            if(selectedScene != null){

                                String newSceneName = args[3];
                                if(SceneManager.sceneExists(newSceneName)){
                                    sender.sendMessage("§cDieser Name für eine Szene ist bereits belegt!");
                                }else{
                                    SceneManager.removeScene(selectedScene.getName());
                                    selectedScene._setName(newSceneName);
                                    SceneManager.addScene(selectedScene);

                                    sender.sendMessage("§aSzene umbenannt in " + selectedScene.getName());
                                }

                            }else{
                                sender.sendMessage("§cDu musst zuerst eine Szene auswählen!");
                            }
                        }else if(args[2].equalsIgnoreCase("permission") || args[2].equalsIgnoreCase("p")){

                            Scene selectedScene = SceneManager.getSelectedScene(sender.getName());
                            if(selectedScene != null){

                                String newPermission = args[3];
                                if(newPermission.equalsIgnoreCase("none")){
                                    newPermission = null;
                                }
                                selectedScene.setRequiredPermission(newPermission);

                                sender.sendMessage("§aPermission geändert zu: §e"  + ((newPermission == null)?"keine":newPermission));

                            }else{
                                sender.sendMessage("§cDu musst zuerst eine Szene auswählen!");
                            }

                        }

                    }else{
                        sender.sendMessage("§c/cinematic edit scene name/permission");
                    }

                }else if(args[1].equalsIgnoreCase("path") || args[1].equalsIgnoreCase("p")){

                    if((args[2].equalsIgnoreCase("name") || args[2].equalsIgnoreCase("n"))&& args.length == 4){

                        Scene selectedScene = SceneManager.getSelectedScene(sender.getName());
                        if(selectedScene != null){

                            CameraPath selectedPath = SceneManager.getSelectedPath(sender.getName());
                            if(selectedPath != null){

                                String newPathName = args[3];
                                if(selectedScene.containsPath(newPathName)){
                                    sender.sendMessage("§cEs gibt bereits einen Pfad mit diesem Namen!");
                                }else{
                                    selectedPath.setName(newPathName);
                                    sender.sendMessage("§aPfad umbenannt in §e" + newPathName);
                                }

                            }else{
                                sender.sendMessage("§cDu musst zuerst einen Pfad auswählen!");
                            }

                        }else{
                            sender.sendMessage("§cDu musst zuerst eine Szene auswählen!");
                        }

                    }else{
                        sender.sendMessage("§c/cinematic edit path name <name>");
                    }

                }else if(args[1].equalsIgnoreCase("position") || args[1].equalsIgnoreCase("pos")) {

                    if (args.length >= 3) {

                        CameraPosition selectedPosition = SceneManager.getSelectedPosition(sender.getName());
                        if (selectedPosition != null) {

                            if (args[2].equalsIgnoreCase("here") || args[2].equalsIgnoreCase("h")) {
                                if (sender instanceof Player) {
                                    selectedPosition.setLocation(p.getLocation().clone());
                                    p.sendMessage("§aPosition geändert zu deinen Koordinaten!");
                                } else {
                                    sender.sendMessage("§cDieser Befehl ist nur für Spieler!");
                                }
                            } else if (args[2].equalsIgnoreCase("time") || args[2].equalsIgnoreCase("t")) {
                                Double newTime = null;

                                if (args.length == 4) {
                                    if (Utils.isDouble(args[3])) {
                                        newTime = Double.valueOf(args[3]);
                                    } else {
                                        sender.sendMessage("§cBitte gib einen valieden Zahlenwert ein!");
                                        return true;
                                    }
                                }

                                selectedPosition.setTimeUntilNext(newTime);
                                sender.sendMessage("§aZeit wurde zu " + newTime + " geändert!");
                            }
                        } else {
                            sender.sendMessage("§cDu musst zuerst eine Position auswählen!");
                        }
                    } else {
                        sender.sendMessage("§c/cinematic edit position <here/time>");
                    }

                }else if(args[1].equalsIgnoreCase("action") || args[1].equalsIgnoreCase("a")){

                    Action selectedAction = SceneManager.getSelectedAction(sender.getName());
                    if(selectedAction != null){

                        if(selectedAction instanceof TeleportPlayerAction){

                            TeleportPlayerAction tpAction = (TeleportPlayerAction)selectedAction;

                            if(args.length >= 3){

                                if(args[2].equalsIgnoreCase("location") || args[2].equalsIgnoreCase("l")){
                                    if(p != null){
                                        tpAction.setLocation(p.getLocation().clone());
                                        p.sendMessage("§alocation wurde zu deiner akuellen Position geändert!");
                                    }else{
                                        sender.sendMessage("§cDieser Befehl ist nur für Spieler!");
                                    }
                                }else if(args[2].equalsIgnoreCase("target") || args[2].equalsIgnoreCase("t")){
                                    if(args.length == 4){
                                        tpAction.setWhom(args[3]);
                                        sender.sendMessage("target wurde zu " + args[3] + " geändert!");
                                    }else{
                                        sender.sendMessage("§c/cinematic edit action target <player/VIEWER/NPC:<npcID>>");
                                    }
                                }else{
                                    sender.sendMessage("§c/cinematic edit action <location/target>");
                                }

                            }else{
                                sender.sendMessage("§c/cinematic edit action <location/target>");
                            }

                        }else if(selectedAction instanceof ChangeGamemodeAction){

                            ChangeGamemodeAction gamemodeAction = (ChangeGamemodeAction)selectedAction;

                            if(args.length >= 3){

                                if(args[2].equalsIgnoreCase("gamemode") || args[2].equalsIgnoreCase("gm")){

                                    if(args.length == 4){

                                        GameMode gameMode = null;
                                        if (args[3].equalsIgnoreCase("0") || args[4].equalsIgnoreCase("survival")) {
                                            gameMode = GameMode.SURVIVAL;
                                        } else if (args[3].equalsIgnoreCase("1") || args[4].equalsIgnoreCase("creative")) {
                                            gameMode = GameMode.CREATIVE;
                                        } else if (args[3].equalsIgnoreCase("2") || args[4].equalsIgnoreCase("adventure")) {
                                            gameMode = GameMode.ADVENTURE;
                                        } else if (args[3].equalsIgnoreCase("3") || args[4].equalsIgnoreCase("spectator")) {
                                            gameMode = GameMode.SPECTATOR;
                                        } else {
                                            sender.sendMessage("§cBitte gib einen gütligen Spielmodus an! <survival/creative/adventure/spectator>");
                                        }

                                        if(gameMode != null){
                                            gamemodeAction.setGamemode(gameMode);
                                            sender.sendMessage("§aSpielmodus wurde zu " + args[3] + " geändert!");
                                        }

                                    }else{
                                        sender.sendMessage("§c/cinematic edit action gamemode <gamemode>");
                                    }

                                }else if(args[2].equalsIgnoreCase("target") || args[2].equalsIgnoreCase("t")){

                                    if(args.length == 4){

                                        gamemodeAction.setWhom(args[3]);
                                        sender.sendMessage("§aSpielmodusempfänger  wurde zu " + args[3] + " gesetzt!");

                                    }else{
                                        sender.sendMessage("§c/cinematic edit action target <target>");
                                    }

                                }else{
                                    sender.sendMessage("§c/cinematic edit action <gamemode/target>");
                                }

                            }else{
                                sender.sendMessage("§c/cinematic edit action <gamemode/target>");
                            }

                        }else if(selectedAction instanceof ExecuteCommandAction){

                            ExecuteCommandAction cmdAction = (ExecuteCommandAction)selectedAction;

                            if(args.length >= 3){

                                if(args[2].equalsIgnoreCase("command") || args[2].equalsIgnoreCase("cmd")){

                                    if(args.length >= 4){

                                        StringBuilder command = new StringBuilder();
                                        for(String arg : Arrays.copyOfRange(args, 4, args.length)){
                                            command.append(" ").append(arg);
                                        }
                                        cmdAction.setCommand(command.toString());
                                        sender.sendMessage("§aBefehl wurde gesetzt zu §e" + command.toString());

                                    }else{
                                        sender.sendMessage("§c/cinematic edit action command <command>");
                                    }

                                }else if(args[2].equalsIgnoreCase("target") || args[2].equalsIgnoreCase("t")){

                                    if(args.length == 4){

                                        cmdAction.setAsWhom(args[3]);
                                        sender.sendMessage("§aBefehlsausführer wurde gesetzt zu " + args[3]);

                                    }else{
                                        sender.sendMessage("§c/cinematic edit action target <target>");
                                    }

                                }else{
                                    sender.sendMessage("§c/cinematic edit action <command/target>");
                                }

                            }else{
                                sender.sendMessage("§c/cinematic edit action <command/target>");
                            }
                        }

                    }else{
                        sender.sendMessage("§cDu musst zuerst eine Aktion auswählen!");
                    }

                }else{
                    sender.sendMessage("§c/cinematic edit <scene/path/position/action>");
                }
            }else{
                sender.sendMessage("§c/cinematic edit <scene/path/position/action>");
            }

        }else if(args[0].equalsIgnoreCase("delete") || args[0].equalsIgnoreCase("d")){

            if(args.length >= 2){
                if(args[1].equalsIgnoreCase("scene") || args[1].equalsIgnoreCase("s")){
                    Scene selectedScene = SceneManager.getSelectedScene(sender.getName());
                    if(selectedScene != null){
                        SceneManager.removeScene(selectedScene.getName());
                        sender.sendMessage("§aSzene gelöscht.");
                        SceneManager.selectScene(sender.getName(), null);
                    }else{
                        sender.sendMessage("§cWähle zuerst eine Szene aus!");
                    }
                }else if(args[1].equalsIgnoreCase("path") || args[1].equalsIgnoreCase("p")){

                    Scene selectedScene = SceneManager.getSelectedScene(sender.getName());
                    if(selectedScene != null){

                        CameraPath selectedPath = SceneManager.getSelectedPath(sender.getName());
                        if(selectedPath != null){

                            selectedScene.removePath(selectedPath);
                            sender.sendMessage("§aPfad gelöscht.");
                            SceneManager.selectPath(sender.getName(), null);
                        }else{
                            sender.sendMessage("§cWähle zuerst einen Pfad aus!");
                        }

                    }else{
                        sender.sendMessage("§cWähle zuerst eine Szene aus!");
                    }

                }else if(args[1].equalsIgnoreCase("position") || args[1].equalsIgnoreCase("pos")) {

                    CameraPath selectedPath = SceneManager.getSelectedPath(sender.getName());
                    if (selectedPath != null) {

                        CameraPosition selectedPosition = SceneManager.getSelectedPosition(sender.getName());
                        if (selectedPosition != null) {
                            selectedPath.removePosition(selectedPosition);
                            sender.sendMessage("§aPosition gelöscht.");
                            SceneManager.selectPosition(sender.getName(), null);
                        } else {
                            sender.sendMessage("§cWähle zuerst eine Position aus!");
                        }

                    } else {
                        sender.sendMessage("§cWähle zuerst einen Pfad aus!");
                    }

                }else if(args[1].equalsIgnoreCase("action") || args[1].equalsIgnoreCase("a")){

                    Action selectedAction = SceneManager.getSelectedAction(sender.getName());
                    if(selectedAction != null){

                        ActionHolder actionHolder = null;

                        CameraPosition pos = SceneManager.getSelectedPosition(sender.getName());
                        if(pos != null){
                            if(pos.containsAction(selectedAction)){
                                actionHolder = pos;
                            }
                        }

                        Scene scene = SceneManager.getSelectedScene(sender.getName());
                        if(scene != null){
                            if(scene.getOnFinish().containsAction(selectedAction)){
                                actionHolder = scene.getOnFinish();
                            }else if(scene.getOnInterrupted().containsAction(selectedAction)){
                                actionHolder = scene.getOnInterrupted();
                            }
                        }

                        if(actionHolder != null){
                            actionHolder.removeAction(selectedAction);
                            sender.sendMessage("§aAktion gelöscht.");
                        }else{
                            sender.sendMessage("§cWähle zuerst eine Aktion aus!");
                        }

                    }else{
                        sender.sendMessage("§cWähle zuerst eine Aktion aus!");
                    }

                }else{
                    sender.sendMessage("§c/cinematic delete <scene/path/position/action>");
                }
            }

        }else if(args[0].equalsIgnoreCase("play") || args[0].equalsIgnoreCase("p")) {

            if(args.length >= 2){
                String sceneName = args[1];
                if (SceneManager.sceneExists(sceneName)) {

                    Player player = null;

                    if (sender instanceof Player) {
                        player = (Player) sender;
                        p = (Player) sender;
                    }

                    if (args.length == 3) {
                        for (Player pl : Bukkit.getServer().getOnlinePlayers()) {
                            if (pl.getName().equalsIgnoreCase(args[2])) {
                                player = pl;
                            }
                        }

                        if (player == p) {
                            sender.sendMessage("§cSpieler nicht gefunden!");
                            return true;
                        }
                    }

                    if (player != null) {

                        SceneViewer.playScene(SceneManager.getScene(sceneName), player);

                    } else {
                        System.err.println("Kein Spieler zum Abspielen der Szene " + sceneName + " gefunden!");
                    }


                } else {
                    sender.sendMessage("§cSzene nicht gefunden!");
                }
            }else{
                sender.sendMessage("§c/cinematic play <scene> [player]");
            }

        }else if(args[0].equalsIgnoreCase("pause") || args[0].equalsIgnoreCase("ps")){

            Player pausedPlayer = null;

            if(args.length == 2){
                pausedPlayer = Bukkit.getPlayer(args[1]);
            }else if(sender instanceof Player){
                pausedPlayer = (Player)sender;
            }

            if(pausedPlayer != null){
                if(SceneViewer.isViewingPath(pausedPlayer)){

                    SceneViewer.pause(pausedPlayer);
                    sender.sendMessage("§aAnsehen der Szene wurde pausiert!");

                }else{
                    sender.sendMessage("§c" + args[1] + " sieht sich grade keine Szene an!");
                }
            }else{
                sender.sendMessage("§c/cinematic pause [player]");
            }
        }else if(args[0].equalsIgnoreCase("resume") || args[0].equalsIgnoreCase("rs")){

            Player resumedPlayer = null;

            if(args.length == 2){
                resumedPlayer = Bukkit.getPlayer(args[1]);
            }else if(sender instanceof Player){
                resumedPlayer = (Player)sender;
            }

            if(resumedPlayer != null){
                if(SceneViewer.isViewingPath(resumedPlayer) || SceneViewer.isPaused(resumedPlayer)){

                    SceneViewer.resume(resumedPlayer);
                    sender.sendMessage("§aDie Szene wird weiter abgespielt!");

                }else{
                    sender.sendMessage("§c" + args[1] + " sieht sich grade keine Szene an oder ist nicht pausiert!");
                }
            }else{
                sender.sendMessage("§c/cinematic resume [player]");
            }

        }else if(args[0].equalsIgnoreCase("stop") || args[0].equalsIgnoreCase("st")) {

            Player stoppedPlayer = null;

            if (args.length == 2) {
                stoppedPlayer = Bukkit.getPlayer(args[1]);
            } else if (sender instanceof Player) {
                stoppedPlayer = (Player) sender;
            }

            if (stoppedPlayer != null) {
                if (SceneViewer.isViewingPath(stoppedPlayer)) {

                    SceneViewer.stopViewingScene(stoppedPlayer);
                    //TODO Maybe execute onInterrupted?
                    sender.sendMessage("§aAnsehen der Szene wurde gestoppt!");

                } else {
                    sender.sendMessage("§c" + args[1] + " sieht sich grade keine Szene an!");
                }
            } else {
                sender.sendMessage("§c/cinematic resume [player]");
            }
        }else if(args[0].equalsIgnoreCase("save") || args[0].equalsIgnoreCase("sv")){

            SceneManager.save();
            sender.sendMessage("§aCinematics gespeichert!");

        }else if(args[0].equalsIgnoreCase("reload") || args[0].equalsIgnoreCase("rl")){
            sender.sendMessage("§cAchtung: Die Nutzung von reload-Befehlen kann zu unvorhersehbaren Bugs führen! Von Benutzung wird abgeraten!");
            SceneManager.reload();
            sender.sendMessage("§aCinematics neugeladen!");

        }else{
            sender.sendMessage("§c/cinematic <create/list/select/edit/delete/play/pause/resume/stop/save/reload>");
        }

        return true;
    }
}
