package de.namnodorel.cinematics.actions;

import de.namnodorel.cinematics.logic.Action;
import net.citizensnpcs.api.CitizensAPI;
import net.citizensnpcs.api.npc.NPC;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.Map;

public class ExecuteCommandAction extends Action{

    private String asWhom;
    private String command;

    public ExecuteCommandAction(String asWhom, String command){
        this.asWhom = asWhom;
        this.command = command.trim();
    }

    @Override
    public void run(NPC cameraNPC, Player p) {

        CommandSender sender = null;
        if(asWhom.equalsIgnoreCase("VIEWER")){
            sender = p;
        }else if(asWhom.equalsIgnoreCase("CONSOLE")){
            sender = Bukkit.getConsoleSender();
        }else if(asWhom.startsWith("NPC:")){
            NPC npc = CitizensAPI.getNPCRegistry().getById(Integer.valueOf(asWhom.replace("NPC:", "")));
            if(npc != null){
                sender = npc.getEntity();
            }
        }else{
            for(Player player : Bukkit.getServer().getOnlinePlayers()){
                if(player.getName().equalsIgnoreCase(asWhom)){
                    sender = player;
                    break;
                }
            }
        }

        if(sender != null){
            Bukkit.getServer().dispatchCommand(sender, command);
        }else{
            System.err.println("[WARNING]: ExecuteCommandAction is unable to find sender to send from for expression: " + asWhom);
        }

    }

    public String getAsWhom() {
        return asWhom;
    }

    public String getCommand() {
        return command;
    }

    public void setAsWhom(String asWhom) {
        this.asWhom = asWhom;
    }

    public void setCommand(String command) {
        this.command = command.trim();
    }

    @Override
    public Map<String, Object> serialize() {
        HashMap<String, Object> serialized = new HashMap<>();
        serialized.put("asWhom", asWhom);
        serialized.put("command", command);
        return serialized;
    }

    public static ExecuteCommandAction deserialize(Map<String, Object> serialized){
        return new ExecuteCommandAction((String)serialized.get("asWhom"), (String)serialized.get("command"));
    }
}
