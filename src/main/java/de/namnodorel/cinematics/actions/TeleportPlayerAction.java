package de.namnodorel.cinematics.actions;

import de.namnodorel.cinematics.logic.Action;
import net.citizensnpcs.api.CitizensAPI;
import net.citizensnpcs.api.npc.NPC;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.Map;

public class TeleportPlayerAction extends Action {

    private String whom;
    private Location location;

    public TeleportPlayerAction(String whom, Location location) {
        this.whom = whom;
        this.location = location;
    }

    @Override
    public void run(NPC cameraNPC, Player p) {

        Entity entityToTeleport = null;

        if(whom.equalsIgnoreCase("VIEWER")){
            entityToTeleport = p;
        }else if(whom.startsWith("NPC:")){
            NPC npc = CitizensAPI.getNPCRegistry().getById(Integer.valueOf(whom.replace("NPC:", "")));
            if(npc != null){
                entityToTeleport = npc.getEntity();
            }
        }else{
            for(Player player : Bukkit.getServer().getOnlinePlayers()){
                if(player.getName().equalsIgnoreCase(whom)){
                    entityToTeleport = player;
                    break;
                }
            }
        }

        if(entityToTeleport != null){
            entityToTeleport.teleport(location);
        }else{
            System.err.println("[WARNING]: TeleportPlayerAction is unable to find entity to teleport for expression: " + whom);
        }
    }

    public void setWhom(String whom) {
        this.whom = whom;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public String getWhom() {
        return whom;
    }

    public Location getLocation() {
        return location;
    }

    @Override
    public Map<String, Object> serialize() {
        HashMap<String, Object> serialized = new HashMap<>();
        serialized.put("whom", whom);
        serialized.put("location", location);
        return serialized;
    }

    public static TeleportPlayerAction deserialize(Map<String, Object> serialized){
        return new TeleportPlayerAction((String)serialized.get("whom"),(Location)serialized.get("location"));
    }

    @Override
    public String toString() {
        return "TeleportPlayerAction{" +
                "whom='" + whom + '\'' +
                ", location=" + location +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TeleportPlayerAction that = (TeleportPlayerAction) o;

        if (!whom.equals(that.whom)) return false;
        return location.equals(that.location);
    }

    @Override
    public int hashCode() {
        int result = whom.hashCode();
        result = 31 * result + location.hashCode();
        return result;
    }
}
