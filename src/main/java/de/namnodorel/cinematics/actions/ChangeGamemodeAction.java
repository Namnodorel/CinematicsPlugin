package de.namnodorel.cinematics.actions;

import de.namnodorel.cinematics.logic.Action;
import net.citizensnpcs.api.CitizensAPI;
import net.citizensnpcs.api.npc.NPC;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.Map;

public class ChangeGamemodeAction extends Action{

    private String whom;
    private GameMode gamemode;

    public ChangeGamemodeAction(String whom, GameMode gamemode){
        this.whom = whom;
        this.gamemode = gamemode;
    }

    @Override
    public void run(NPC cameraNPC, Player p) {

        Player playerToChangeGamemode = null;

        if(whom.equalsIgnoreCase("VIEWER")){
            playerToChangeGamemode = p;
        }else if(whom.startsWith("NPC:")){
            NPC npc = CitizensAPI.getNPCRegistry().getById(Integer.valueOf(whom.replace("NPC:", "")));
            if(npc != null && npc.getEntity().getType().equals(EntityType.PLAYER)){
                playerToChangeGamemode = (Player)npc.getEntity();
            }
        }else{
            for(Player player : Bukkit.getServer().getOnlinePlayers()){
                if(player.getName().equalsIgnoreCase(whom)){
                    playerToChangeGamemode = player;
                    break;
                }
            }
        }

        if(playerToChangeGamemode != null){
            playerToChangeGamemode.setGameMode(gamemode);
        }else{
            System.err.println("[WARNING]: ChangeGamemodeAction is unable to find player to change gamemode of for expression: " + whom);
        }

    }

    public String getWhom() {
        return whom;
    }

    public GameMode getGamemode() {
        return gamemode;
    }

    public void setWhom(String whom) {
        this.whom = whom;
    }

    public void setGamemode(GameMode gamemode) {
        this.gamemode = gamemode;
    }

    @Override
    public Map<String, Object> serialize() {
        HashMap<String, Object> serialized = new HashMap<>();
        serialized.put("whom", whom);
        serialized.put("gamemode", gamemode.name());
        return serialized;
    }

    public static ChangeGamemodeAction deserialize(Map<String, Object> serialized){
        return new ChangeGamemodeAction((String)serialized.get("whom"), GameMode.valueOf((String)serialized.get("gamemode")));
    }
}
