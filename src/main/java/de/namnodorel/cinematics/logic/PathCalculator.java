package de.namnodorel.cinematics.logic;

import org.apache.commons.math3.analysis.interpolation.SplineInterpolator;
import org.apache.commons.math3.analysis.polynomials.PolynomialSplineFunction;
import org.bukkit.Location;

import java.util.ArrayList;
import java.util.List;

public class PathCalculator {
    public static List<Location> calculateLocations(List<CameraPosition> positions, Integer pointsPerSecond){
        List<Location> locations = new ArrayList<>();

        double[] time = new double[positions.size()];

        double totalTime = 0;
        for(CameraPosition p : positions){
            time[positions.indexOf(p)] = totalTime;
            totalTime += (p.getTimeUntilNext() == null?1:p.getTimeUntilNext())*pointsPerSecond;
        }

        double[] xValues = new double[positions.size()];
        for(CameraPosition p : positions){
            xValues[positions.indexOf(p)] = p.getLocation().getX();
        }

        double[] yValues = new double[positions.size()];
        for(CameraPosition p : positions){
            yValues[positions.indexOf(p)] = p.getLocation().getY();
        }

        double[] zValues = new double[positions.size()];
        for(CameraPosition p : positions){
            zValues[positions.indexOf(p)] = p.getLocation().getZ();
        }

        //The SplineInterpolator doesn't work very well with the edge of 360, so we
        //correct that for the purpose of calculation. Due to this, turns with more than 180 degrees
        //difference need to be created with more positions
        double[] yawValues = new double[positions.size()];
        Double lastYaw = null;
        Integer overwinded = 0;
        for(CameraPosition p : positions){
            double yaw  = (p.getLocation().getYaw());
            if(lastYaw != null){
                double diff = lastYaw - yaw;
                if(diff > 180){
                    overwinded++;
                }else if(diff < -180){
                    overwinded--;
                }
            }

            yawValues[positions.indexOf(p)] = yaw+(360*overwinded);
            lastYaw = yaw;
        }

        double[] pitchValues = new double[positions.size()];
        for(CameraPosition p : positions){
            pitchValues[positions.indexOf(p)] = (p.getLocation().getPitch());
        }

        SplineInterpolator interpolator = new SplineInterpolator();

        PolynomialSplineFunction xFunc = interpolator.interpolate(time, xValues);
        PolynomialSplineFunction yFunc = interpolator.interpolate(time, yValues);
        PolynomialSplineFunction zFunc = interpolator.interpolate(time, zValues);
        PolynomialSplineFunction yawFunc = interpolator.interpolate(time, yawValues);
        PolynomialSplineFunction pitchFunc = interpolator.interpolate(time, pitchValues);

        try {

            for(double i = 0; i < totalTime - (((positions.get(positions.size() - 1).getTimeUntilNext() == null)?1:positions.get(positions.size() - 1).getTimeUntilNext())*20); i++){

                Location loc = new Location(positions.get(0).getLocation().getWorld(), xFunc.value(i), yFunc.value(i), zFunc.value(i));
                double yaw = yawFunc.value(i);
                //yaws.add(yaw);
                loc.setYaw((float) (yaw));

                double pitch = pitchFunc.value(i);
                loc.setPitch((float) (pitch ));

                locations.add(loc);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return locations;
    }

}
