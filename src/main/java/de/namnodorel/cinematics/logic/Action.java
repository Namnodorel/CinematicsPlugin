package de.namnodorel.cinematics.logic;

import net.citizensnpcs.api.npc.NPC;
import org.bukkit.configuration.serialization.ConfigurationSerializable;
import org.bukkit.entity.Player;

public abstract class Action implements ConfigurationSerializable{
    public abstract void run(NPC cameraNPC, Player p);
}
