package de.namnodorel.cinematics.logic;

import net.citizensnpcs.api.npc.NPC;
import org.bukkit.configuration.serialization.ConfigurationSerializable;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ActionHolder implements ConfigurationSerializable{

    List<Action> actions = new ArrayList<>();

    public void addAction(Action a){
        actions.add(a);
    }

    public boolean containsAction(Action action){
        return getActions().contains(action);
    }

    public void removeAction(Action action){
        if(containsAction(action)){
            getActions().remove(action);
        }else{
            throw new IllegalArgumentException("Cannot delete non-existent action " + action);
        }
    }

    public Action getAction(Integer index){
        return actions.get(index);
    }

    public List<Action> getActions() {
        return actions;
    }


    public void runActions(NPC cameraNPC, Player p){
        for(Action a : actions){
            a.run(cameraNPC, p);
        }
    }

    @Override
    public Map<String, Object> serialize() {
        HashMap<String, Object> serialized = new HashMap<>();
        serialized.put("actions", actions);
        return serialized;
    }

    public static ActionHolder deserialize(Map<String, Object> serialized){
        ActionHolder actionHolder = new ActionHolder();
        actionHolder.actions = (List<Action>)serialized.get("actions");
        return actionHolder;
    }
}
