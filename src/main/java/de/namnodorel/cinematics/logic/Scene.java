package de.namnodorel.cinematics.logic;


import org.bukkit.configuration.serialization.ConfigurationSerializable;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Scene implements ConfigurationSerializable{

    private String name;
    private List<CameraPath> paths;
    private String requiredPermission;

    private ActionHolder onInterrupted = new ActionHolder();
    private ActionHolder onFinish = new ActionHolder();

    public Scene(String name) {
        this.name = name;
        this.paths = new ArrayList<>();
        this.requiredPermission = null;
    }

    public Scene(String name, String requiredPermission){
        this.name = name;
        this.paths = new ArrayList<>();
        this.requiredPermission = requiredPermission;
    }

    public void addPath(CameraPath path){
        if(containsPath(path.getName())){
            throw new IllegalArgumentException("A path with the name'" + path.getName() + "' already exists!");
        }else{
            paths.add(path);
        }
    }

    public List<CameraPath> getPaths() {
        return paths;
    }

    public boolean containsPath(String pathName){
        for(CameraPath path : paths){
            if(path.getName().equalsIgnoreCase(pathName)){
                return true;
            }
        }

        return false;
    }

    public CameraPath getPath(String pathName){
        for(CameraPath path : paths){
            if(path.getName().equalsIgnoreCase(pathName)){
                return path;
            }
        }

        return null;
    }

    public void removePath(CameraPath path){
        if(containsPath(path.getName())){
            paths.remove(path);
        }else{
            throw new IllegalArgumentException("Cannot remove non-existent path " + path.getName());
        }
    }

    public boolean hasPaths(){
        return paths.size() > 0;
    }

    public String getName() {
        return name;
    }

    public void _setName(String name){
        this.name = name;
    }

    public void setRequiredPermission(String permission){
        this.requiredPermission = permission;
    }

    public String getRequiredPermission() {
        return requiredPermission;
    }

    public ActionHolder getOnInterrupted() {
        return onInterrupted;
    }

    public ActionHolder getOnFinish() {
        return onFinish;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Scene scene = (Scene) o;

        if (!getName().equals(scene.getName())) return false;
        if (!getPaths().equals(scene.getPaths())) return false;
        return getRequiredPermission() != null ? getRequiredPermission().equals(scene.getRequiredPermission()) : scene.getRequiredPermission() == null;
    }

    @Override
    public int hashCode() {
        int result = getName().hashCode();
        result = 31 * result + getPaths().hashCode();
        result = 31 * result + (getRequiredPermission() != null ? getRequiredPermission().hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Scene{" +
                "name='" + name + '\'' +
                ", paths=" + paths +
                ", requiredPermission='" + requiredPermission + '\'' +
                '}';
    }

    @Override
    public Map<String, Object> serialize() {
        HashMap<String, Object> serialized = new HashMap<>();
        serialized.put("name", name);
        serialized.put("paths", paths);
        serialized.put("permission", requiredPermission);
        serialized.put("onInterrupted", onInterrupted);
        serialized.put("onFinish", onFinish);
        return serialized;
    }

    public static Scene deserialize(Map<String, Object> serialized){
        Scene scene = new Scene((String)serialized.get("name"), (String)serialized.get("permission"));

        scene.paths = (List<CameraPath>) serialized.get("paths");
        scene.onInterrupted = (ActionHolder)serialized.get("onInterrupted");
        scene.onFinish = (ActionHolder)serialized.get("onFinish");

        return scene;
    }
}
