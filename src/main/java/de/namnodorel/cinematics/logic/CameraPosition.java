package de.namnodorel.cinematics.logic;


import org.bukkit.Location;
import org.bukkit.configuration.serialization.ConfigurationSerializable;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CameraPosition extends ActionHolder implements ConfigurationSerializable {

    private Location location;
    private Double timeUntilNext;

    public CameraPosition(Location location, Double timeUntilNext) {
        this.location = location;
        this.timeUntilNext = timeUntilNext;
    }

    public Location getLocation() {
        return location;
    }

    public Double getTimeUntilNext() {
        return timeUntilNext;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public void setTimeUntilNext(Double timeUntilNext) {
        this.timeUntilNext = timeUntilNext;
    }

    @Override
    public Map<String, Object> serialize() {
        HashMap<String, Object> serialized = new HashMap<>();

        serialized.put("location", location);
        serialized.put("timeUntilNext", timeUntilNext);
        serialized.put("actions", actions);

        return serialized;
    }

    public static CameraPosition deserialize(Map<String, Object> serialized){
        CameraPosition position = new CameraPosition((Location)serialized.get("location"), (Double)serialized.get("timeUntilNext"));

        position.actions = (List<Action>)serialized.get("actions");

        return position;
    }


    @Override
    public String toString() {
        return "CameraPosition{" +
                "location=" + location +
                ", timeUntilNext=" + timeUntilNext +
                ", actions=" + actions +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CameraPosition position = (CameraPosition) o;

        if (!getLocation().equals(position.getLocation())) return false;
        if (getTimeUntilNext() != null ? !getTimeUntilNext().equals(position.getTimeUntilNext()) : position.getTimeUntilNext() != null)
            return false;
        return actions != null ? actions.equals(position.actions) : position.actions == null;
    }

    @Override
    public int hashCode() {
        int result = getLocation().hashCode();
        result = 31 * result + (getTimeUntilNext() != null ? getTimeUntilNext().hashCode() : 0);
        result = 31 * result + (actions != null ? actions.hashCode() : 0);
        return result;
    }
}
