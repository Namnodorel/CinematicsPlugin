package de.namnodorel.cinematics.logic;


import org.bukkit.configuration.serialization.ConfigurationSerializable;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CameraPath implements ConfigurationSerializable{

    private String name;
    private List<CameraPosition> positions;


    public CameraPath(String name){
        this.name = name;
        this.positions = new ArrayList<>();
    }

    public Integer addPosition(CameraPosition pos){
        positions.add(pos);

        return positions.indexOf(pos);
    }

    public List<CameraPosition> getPositions() {
        return positions;
    }

    public String getName() {
        return name;
    }

    public CameraPosition getPosition(Integer positionIndex) {

        if(getPositions().size() - 1 < positionIndex){
            throw new IllegalArgumentException("Tried to get a position with a higher index(" + positionIndex + ") than possible (" + (getPositions().size() - 1) + ")");
        }

        return getPositions().get(positionIndex);
    }

    public void removePosition(CameraPosition pos){
        if(getPositions().contains(pos)){
            getPositions().remove(pos);
        }else{
           throw new IllegalArgumentException("Cannot delete non-existent position " + pos);
        }
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CameraPath that = (CameraPath) o;

        if (!getName().equals(that.getName())) return false;
        return getPositions().equals(that.getPositions());
    }

    @Override
    public int hashCode() {
        int result = getName().hashCode();
        result = 31 * result + getPositions().hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "CameraPath{" +
                "name='" + name + '\'' +
                ", positions=" + positions +
                '}';
    }

    @Override
    public Map<String, Object> serialize() {
        HashMap<String, Object> serialized = new HashMap<>();

        serialized.put("name", name);
        serialized.put("positions", positions);

        return serialized;
    }

    public static CameraPath deserialize(Map<String, Object> serialized){
        CameraPath path = new CameraPath((String)serialized.get("name"));

        path.positions = (List<CameraPosition>) serialized.get("positions");

        return path;
    }

    /*public Integer getIndexOf(CameraPosition pos){

        if(!positions.contains(pos)){
            return null;
        }

        return positions.indexOf(pos);
    }*/
}
