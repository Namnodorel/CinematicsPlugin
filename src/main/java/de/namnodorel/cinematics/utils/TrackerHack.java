package de.namnodorel.cinematics.utils;

import net.minecraft.server.v1_11_R1.*;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerVelocityEvent;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.List;

public class TrackerHack {

    private static Field trackerField;
    private static Field yRotField;
    private static Field xRotField;
    private static Field xLocField;
    private static Field yLocField;
    private static Field zLocField;
    private static Field headYawField;

    static {
        try{
            trackerField = EntityTrackerEntry.class.getDeclaredField("tracker");
            trackerField.setAccessible(true);

            yRotField = EntityTrackerEntry.class.getDeclaredField("yRot");
            yRotField.setAccessible(true);

            xRotField = EntityTrackerEntry.class.getDeclaredField("xRot");
            xRotField.setAccessible(true);

            xLocField = EntityTrackerEntry.class.getDeclaredField("xLoc");
            xLocField.setAccessible(true);

            yLocField = EntityTrackerEntry.class.getDeclaredField("yLoc");
            yLocField.setAccessible(true);

            zLocField = EntityTrackerEntry.class.getDeclaredField("zLoc");
            zLocField.setAccessible(true);

            headYawField = EntityTrackerEntry.class.getDeclaredField("headYaw");
            headYawField.setAccessible(true);
        }catch (Exception ex){
            System.err.println("FATAL ERROR: Could not get fields for CinematicPlugin->TrackerHack! Is the server version correct?");
            ex.printStackTrace();
            System.exit(1);
        }
    }

    public static void track(EntityTrackerEntry entityTrackerEntry) throws Exception{

        Entity tracker = (Entity) trackerField.get(entityTrackerEntry);
        int yRot = (int) yRotField.get(entityTrackerEntry);
        int xRot = (int) xRotField.get(entityTrackerEntry);
        long xLoc = (long) xLocField.get(entityTrackerEntry);
        long yLoc =  (long) yLocField.get(entityTrackerEntry);
        long zLoc = (long) zLocField.get(entityTrackerEntry);
        int headYaw = (int) headYawField.get(entityTrackerEntry);

        int i;

        long k = EntityTracker.a(tracker.locX);
        long l = EntityTracker.a(tracker.locY);
        long i1 = EntityTracker.a(tracker.locZ);
        int j1 = MathHelper.d(tracker.yaw * 256.0F / 360.0F);
        int k1 = MathHelper.d(tracker.pitch * 256.0F / 360.0F);
        long l1 = k - xLoc;
        long i2 = l - yLoc;
        long j2 = i1 - zLoc;

        boolean flag1 = l1 * l1 + i2 * i2 + j2 * j2 >= 128L || entityTrackerEntry.a % 60 == 0;
        boolean flag2 = Math.abs(j1 - yRot) >= 1 || Math.abs(k1 - xRot) >= 1;


        if (flag1) {
            xLoc = k;
            xLocField.set(entityTrackerEntry, xLoc);

            yLoc = l;
            yLocField.set(entityTrackerEntry, yLoc);

            zLoc = i1;
            zLocField.set(entityTrackerEntry, zLoc);
        }

        if (flag2) {
            yRot = j1;
            yRotField.set(entityTrackerEntry, yRot);

            xRot = k1;
            xRotField.set(entityTrackerEntry, xRot);

        }

        Packet packet = new PacketPlayOutEntity.PacketPlayOutRelEntityMoveLook(tracker.getId(), l1, i2, j2, (byte) j1, (byte) k1, tracker.onGround);
        Packet packet2 = new PacketPlayOutEntity.PacketPlayOutEntityLook(tracker.getId(), (byte)j1, (byte)k1, tracker.onGround);

        entityTrackerEntry.broadcast(packet);
        //entityTrackerEntry.broadcast(packet2);

        i = MathHelper.d(tracker.getHeadRotation() * 256.0F / 360.0F);
        if (Math.abs(i - headYaw) >= 1) {
            entityTrackerEntry.broadcast(new PacketPlayOutEntityHeadRotation(tracker, (byte) i));
            headYaw = i;
            headYawField.set(entityTrackerEntry, headYaw);
        }

        tracker.impulse = false;


        ++entityTrackerEntry.a;
        //if (tracker.velocityChanged) {

            boolean cancelled = false;

            if (tracker instanceof EntityPlayer) {
                Player player = (Player) tracker.getBukkitEntity();
                org.bukkit.util.Vector velocity = player.getVelocity();

                PlayerVelocityEvent event = new PlayerVelocityEvent(player, velocity.clone());
                tracker.world.getServer().getPluginManager().callEvent(event);

                if (event.isCancelled()) {
                    cancelled = true;
                } else if (!velocity.equals(event.getVelocity())) {
                    player.setVelocity(event.getVelocity());
                }
            }

            if (!cancelled) {
                entityTrackerEntry.broadcastIncludingSelf(new PacketPlayOutEntityVelocity(tracker));
            }
            tracker.velocityChanged = false;
        //}

    }

}
