package de.namnodorel.cinematics.utils;

import net.citizensnpcs.api.npc.NPC;
import net.minecraft.server.v1_11_R1.EntityTracker;
import net.minecraft.server.v1_11_R1.EntityTrackerEntry;
import net.minecraft.server.v1_11_R1.MinecraftServer;
import net.minecraft.server.v1_11_R1.WorldServer;
import org.objenesis.Objenesis;
import org.objenesis.ObjenesisStd;
import org.objenesis.instantiator.ObjectInstantiator;

import java.lang.reflect.Field;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class PacketHelper {

    //TODO Cleanup, sending packets faster than 20p/s doesn't have a big effect when nothing but bytes are available
    public static void packetUpdate(NPC cameraNPC){
        try {

            Field cField = EntityTracker.class.getDeclaredField("c");
            cField.setAccessible(true);

            for(WorldServer ws : MinecraftServer.getServer().worlds){
                if(ws.getWorld().getName().equalsIgnoreCase(cameraNPC.getEntity().getWorld().getName())){

                    Set<EntityTrackerEntry> stuff = ((Set<EntityTrackerEntry>)cField.get(ws.getTracker()));
                    Iterator iterator = stuff.iterator();

                    while(iterator.hasNext()) {
                        EntityTrackerEntry entitytrackerentry = (EntityTrackerEntry)iterator.next();

                        if(entitytrackerentry.b().getUniqueID().equals(cameraNPC.getEntity().getUniqueId())){
                            try{
                                TrackerHack.track(entitytrackerentry);
                            }catch(Exception ex){
                                ex.printStackTrace();
                            }
                        }
                    }

                }
            }

        }catch (Exception ex){
            ex.printStackTrace();
        }
    }
}
